var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var mainBowerFiles = require('gulp-main-bower-files');
var uglify = require('gulp-uglify');
var gulpFilter = require('gulp-filter');


var paths = {
    scss: './scss/'
};




gulp.task('bower', function() {

    var filterJS = gulpFilter('**/*.js');

    return gulp.src('./bower.json')
        .pipe(mainBowerFiles())
        .pipe(filterJS)
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./lib'))
});



gulp.task('sass', function () {
    return gulp.src([
        paths.scss + 'main.scss'
    ])
    .pipe(sass({
        outputStyle: 'compact',
        includePaths: require('node-bourbon').includePaths
    }))
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream())
});


gulp.task('dev', ['bower', 'sass'], function() { 
    browserSync.init({
        server: './'
    });
   // gulp.watch('bower_components/**', ['bower']);
    gulp.watch(paths.scss + '**/*.scss', ['sass']);  
    gulp.watch(["**/*.html", '**/*.js']).on('change', browserSync.reload); 
});

gulp.task('build', ['sass'],  function () {
  return gulp.src('./src/*.js')
    .pipe(concat(pkg.name + '.js'))
    .pipe(gulp.dest('./dist'))
    .pipe(rename(pkg.name + '.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));
});
