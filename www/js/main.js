$(document).ready(function(){

	var validator = $("#SubcribeForm").validate({
			rules: {
				LastName: {
					required: true,
					minlength: 2
				},
				FirstName: {
					required: true,
					minlength: 2
				},
				InternetAddress: {
					required: true,
					email: true
				},
				Agree: {
					required: true
				}
			},
			messages: {
				LastName: {
					required: "A Vezetéknév mező kitöltése kötelező!",
					minlength: jQuery.validator.format("A vezetéknév legalább {0} karaktert tartalmazzon!"),
				},
				FirstName: {
					required: "A keresztnév mező kitöltése kötelező!",
					minlength: jQuery.validator.format("A keresztnév legalább {0} karaktert tartalmazzon!"),
				},
				InternetAddress: {
					required: "Az e-mail cím mező kitöltése kötelező",
					email: "Kérjük adjon meg érvényes e-mail címet!"
				},
				Agree: {
					required: "A Hozzájárulása szükséges a regisztrációhoz!"
				}
			},
			errorPlacement: function(error, element) {
				if (element.is(":radio"))
					error.appendTo(element.parent().next().next());
				else if (element.is(":checkbox"))
					error.appendTo(element.parent().parent());
				else
					error.appendTo(element.parent());
			},

			submitHandler: function() {

				$.post( "https://extranet.toyota.hu/portal/edm_pub.nsf/Subscribe?Open", $( "#SubcribeForm" ).serialize(), function(data){
					if (data.substr(0,2)=='OK') { 
						$('#thankyou').slideDown(); 
						$( "#SubcribeForm" ).reset();
					} else { 
						alert('Hiba: ' + data); 
						$('#RegisterButton').slideDown(); 
					} 
				});
			},
			// set this class to error-labels to indicate valid fields
			success: function(label) {
				// set &nbsp; as text for IE
				label.html("&nbsp;").addClass("checked");
			},
			highlight: function(element, errorClass) {
				$(element).parent().next().find("." + errorClass).removeClass("checked");
			}
		});


});
